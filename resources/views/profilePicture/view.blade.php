<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Name Add Form</title>
    <link rel="stylesheet" href="{{ asset('public/atomicProject/bootstrap/css/bootstrap.min.css')}}">
    <script src="{{ asset('public/atomicProject/bootstrap/js/jquery.js')}}"></script>
    <style>
        body{
            background-image: url("{{ asset('public/atomicProject/img/b.jpg')}}");
        }
        * {
            margin: 0;
            padding: 0;
        }

        form {
            width: 700px;
            padding: 20px;
            /*border-radius: 0 0 15px 15px;*/
            margin: 0 auto;
            margin-top: 8%;
        }
        .content{
            margin-top: 15%;

        }

        #message {
            width: 700px;
            text-align: center;
            margin: 0 auto;
            display: none;

        }

        .formInfo {
            margin: 0 auto;
            border: 1px solid #2a9fb4;
            background-color: #1abc9c;
            width: 702px;
            box-shadow: 0 0 2px;
            border-radius: 12px 12px 15px 15px;
        }


        main {
            margin-top: 50px;
        }

    </style>
</head>
<body>

@include('header.header')

<div class="container content">

    <table class="table table-boarderd">
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Image</th>
            <th>Publication Status</th>
        </tr>
        <tr>
            <td>{{$single_info->id}}</td>
            <td>{{$single_info->name}}</td>
            <td><img src="{{asset($single_info->image)}}" height="100px" width="100px"></td>
            <td>
                @if($single_info->is_trashed=='no')
                  Published
                @else
                  Unpublished
                @endif

            </td>
        </tr>


    </table>



</div>



</body>
</html>
