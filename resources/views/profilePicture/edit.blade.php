<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Name Add Form</title>
    <link rel="stylesheet" href="{{ asset('public/atomicProject/bootstrap/css/bootstrap.min.css')}}">
    <script src="{{ asset('public/atomicProject/bootstrap/js/jquery.js')}}"></script>
    <style>
        body{
            background-image: url("{{ asset('public/atomicProject/img/b.jpg')}}");
        }
        * {
            margin: 0;
            padding: 0;
        }

        form {
            width: 700px;
            padding: 20px;
            /*border-radius: 0 0 15px 15px;*/
            margin: 0 auto;
            margin-top: 8%;
        }
        .content{
            margin-top: 15%;

        }

        #message {
            width: 700px;
            text-align: center;
            margin: 0 auto;
            display: none;

        }

        .formInfo {
            margin: 0 auto;
            border: 1px solid #2a9fb4;
            background-color: #1abc9c;
            width: 702px;
            box-shadow: 0 0 2px;
            border-radius: 12px 12px 15px 15px;
        }


        main {
            margin-top: 50px;
        }

    </style>
</head>
<body>

@include('header.header')

<div class="container content">




    <div class="main">
        <h1 class="text-center">Profile Image Update</h1>

        <div class="formInfo">
            {!! Form::open(['url' => '/ppupdate','method'=>'post','class'=> 'well form-horizontal','enctype'=>'multipart/form-data']) !!}
            @if ($errors->any())
                <div class="alert alert-danger" id="message">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="alert alert-success" id="message">
                {{Session::get('success'),Session::put('success',null)}}
            </div>


            <div class="form-group" align="center">
                <label class="control-label col-sm-2"  for="name">Name<em>*</em></label>
                <div class="col-sm-6">
                    <input type="text" name="name" id="name" value="{{$single_info->name}}" class="form-control"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="image"> Profile Picture</label>
                <div class="col-sm-6">
                    <input type="file" name="image" id="image"   class="form-control"/>
                    <img src="{{asset($single_info->image)}}" height="100px" width="100px">
                </div>
                <input type="hidden" name="id" value="{{$single_info->id}}">
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <input type="submit" name="submit" id="submit" value="Submit"  class="btn btn-success"/>
                    <a href="{{url('/ppindex')}}" class="btn btn-info">View</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>






<script>


    jQuery(

        function($) {
            $('#message').fadeIn (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>



</body>
</html>
