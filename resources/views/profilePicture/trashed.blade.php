<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>Image Index</title>

    <link rel="stylesheet" href="{{ asset('public/atomicProject/bootstrap/css/bootstrap.min.css')}}">
    <script src="{{ asset('public/atomicProject/bootstrap/js/jquery.js')}}"></script>






</head>
<body>
<style>
    body{
        background-image: url("{{ asset('public/atomicProject/img/b.jpg')}}");
    }
</style>


@include('header.header')

<!-- required for search, block 4 of 5 start -->
<div class="nav navbar-nav pull-right">
    <form id="searchForm" action="index.php"  method="get" style="margin-top: 5px; margin-bottom: 10px ">
        <input type="text" value="" id="searchID" name="search" placeholder="Search" width="60" >
        <input type="checkbox"  name="byTitle"   checked  >By Title
        <input type="checkbox"  name="byAuthor"  checked >By Author
        <input hidden type="submit" class="btn-primary" value="search">
    </form>
</div>

<!-- required for search, block 4 of 5 end -->

<div >
    <a href="{{url('/pp')}}" class="btn btn-primary"> Create</a>
</div>

<?php if (isset($msg)) {
    echo "<div id='message'>$msg</div>";
}?>

<h1>  Image Trashed Information</h1>

{!! Form::open(['url' => '/pprecover_multiple','method'=>'post','id'=>'selectionForm']) !!}



<div class="nav navbar">
    <a href="{{url('/ppindex')}}" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-list-alt"></span> <span class="glyphicon glyphicon-circle-arrow-down"></span>Index List</a>
    <input class="btn btn-warning btn-lg" type="button" id="trashMultipleButton" value="Recover Multiple">
    <input class="btn  btn-danger btn-lg" type="button" id="deleteMultipleButton" value="Delete Multiple">

    <a href="{{url('/pppdf')}}" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-list-alt"></span> <span class="glyphicon glyphicon-circle-arrow-down"></span> Download as PDF</a>
    <a href="{{url('/ppxl')}}" class="btn btn-lg btn-info"><span class="glyphicon glyphicon-list-alt"></span> <span class="glyphicon glyphicon-circle-arrow-down"></span> Download as Excel</a>
    <a href="{{url('/ppmail')}}" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-list-alt"></span> <span class="glyphicon glyphicon-envelope"></span> Email This List</a>


</div>


<table class="table table-bordered table-striped" border="2">

    <tr>
        <th>Check All <input type='checkbox' id='select_all' name='select_all' value='$record->id'></th>
        <th>Serial </th>
        <th>ID</th>
        <th>Name</th>
        <th>Image</th>
        <th>Actions</th>

    </tr>
    <?php $serial=1?>
    @foreach($image_info as $value)

        <tr>
            <td><input type='checkbox' class='checkbox' name='multiple[]' value='{{$value->id}}'> </td>
            <td><?php echo $serial?></td>
            <td>{{$value->id}}</td>
            <td>{{$value->name}}</td>
            <td><img src="{{$value->image}}" height="100px" width="100px"></td>
            <td>

                <a href='{{url('/ppview/'.$value->id)}}' class='btn btn-primary'> View </a>
                <a href='{{url('/ppedit/'.$value->id)}}' class='btn btn-success'> Edit </a>
                <a href='{{url('/pprecover/'.$value->id)}}' class='btn btn-warning'> Recover </a>
                <a href='{{url('/ppdelete/'.$value->id)}}' onclick='return confirm_delete()' class='btn btn-danger'> Delete </a>
                <a href='email.php?id=$record->id' class='btn btn-primary'> <span class='glyphicon glyphicon-envelope'> </span> Email This Record </a>

            </td>

        </tr>
        <?php $serial++?>
    @endforeach
    {{ $image_info->links() }}


</table>
{!! Form::close() !!}






<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>

<script>

    function confirm_delete(){

        return confirm("Are You Sure?");

    }

</script>


<script>

    $('#deleteMultipleButton').click(function(){

        if(checkEmptySelection()){
            alert("Empty Selection! Please select some record(s) first")
        }
        else{
            var r = confirm("Are you sure you want to delete the selected record(s)?");

            if(r){
                var selectionForm =   $('#selectionForm');
                selectionForm.attr("action","{{url('/ppdelete_multiple')}}");
                selectionForm.submit();
            }
        }
    });


</script>



<script>

    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){ //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

//check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });



</script>

<script>

    function checkEmptySelection(){

        emptySelection =true;

        $('.checkbox').each(function(){
            if(this.checked)   emptySelection = false;
        });

        return emptySelection;
    }


    $("#trashMultipleButton").click(function(){

        if(checkEmptySelection()){
            alert("Empty Selection! Please select some record(s) first")
        }else{

            $("#selectionForm").submit();

        }




    }) ;



</script>



<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <!--            --><?php
            //            echo $comma_separated_keywords;
            //            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block 5 of 5 end -->


</body>
</html>
