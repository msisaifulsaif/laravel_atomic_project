<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style>
        table{
            border-collapse: collapse;
        }
        td,th{

            border: 1px solid;
        }
    </style>
</head>
<body>
<table >
    <tr>
        <th width="25%">Sl</th>
        <th width="25%">ID</th>
        <th width="25%">Name</th>
        <th width="25%">Image</th>
    </tr>
    <?php $serial=1?>
    @foreach($book_info as $value)
        <tr>
            <td style="color: #0000cc ">{{$serial}}</td>
            <td style="color: #0000cc">{{$value->id}}</td>
            <td style="color: #0000cc">{{$value->name}}</td>
            <td style="color: #0000cc"><img src="{{asset($value->image)}}" width="100px" height="100px"></td>
        </tr>

        <?php $serial++?>
        @endforeach
</table>
</body>
</html>