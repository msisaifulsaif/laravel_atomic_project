<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style>
        table{
            border-collapse: collapse;
        }
        td,th{

            border: 1px solid;
        }
    </style>
</head>
<body>
<table >
    <tr>
        <th width="25%">Sl</th>
        <th width="25%">ID</th>
        <th width="25%">Book Name</th>
        <th width="25%">Author Name</th>
    </tr>
    <?php $serial=1?>
    @foreach($book_info as $value)
        <tr>
            <td style="color: #0000cc ">{{$serial}}</td>
            <td style="color: #0000cc">{{$value->id}}</td>
            <td style="color: #0000cc">{{$value->book_name}}</td>
            <td style="color: #0000cc">{{$value->author_name}}</td>
        </tr>

        <?php $serial++?>
        @endforeach
</table>
</body>
</html>