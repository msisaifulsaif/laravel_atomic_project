<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Name Add Form</title>
    <link rel="stylesheet" href="{{ asset('public/atomicProject/bootstrap/css/bootstrap.min.css')}}">
    <script src="{{ asset('public/atomicProject/bootstrap/js/jquery.js')}}"></script>
    <style>
        body{
            background-image: url("{{ asset('public/atomicProject/img/b.jpg')}}");
        }
        * {
            margin: 0;
            padding: 0;
        }

        form {
            width: 700px;
            padding: 20px;
            /*border-radius: 0 0 15px 15px;*/
            margin: 0 auto;
            margin-top: 8%;
        }
        .content{
            margin-top: 15%;

        }

        #message {
            width: 700px;
            text-align: center;
            margin: 0 auto;
            display: none;

        }

        .formInfo {
            margin: 0 auto;
            border: 1px solid #2a9fb4;
            background-color: #1abc9c;
            width: 702px;
            box-shadow: 0 0 2px;
            border-radius: 12px 12px 15px 15px;
        }


        main {
            margin-top: 50px;
        }

    </style>
</head>
<body>

@include('header.header')

<div class="container content">




    <div class="main">
        <h1 class="text-center">Book Name Entry</h1>

        <div class="formInfo">
                {!! Form::open(['url' => '/updateBook','method'=>'post','class'=> 'well form-horizontal']) !!}

            @if ($errors->any())
                <div class="alert alert-danger" id="message">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        <div class="alert alert-success" id="message">
            {{Session::get('success'),Session::put('success',null)}}
        </div>


                <div class="form-group" align="center">
                    <label class="control-label col-sm-2"  for="bookTitle">Book Title<em>*</em></label>
                    <div class="col-sm-6">
                        <input type="text" name="bookname" id="bookTitle" value="{{$single_info->book_name}}" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="authorname"> Author Name</label>
                    <div class="col-sm-6">
                        <input type="text" name="authorname" id="authorname"  value="{{$single_info->author_name}}"  class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <input type="hidden" name="id" id="id" value="{{$single_info->id}}" class="form-control"/>
                    <div class="col-sm-offset-2 col-sm-8">
                        <input type="submit" name="submit" id="submit" value="Submit"  class="btn btn-success"/>
                        <a href="{{url('/index')}}" class="btn btn-info">View</a>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>






<script>


    jQuery(

        function($) {
            $('#message').fadeIn (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>



</body>
</html>
