<?php

namespace App\Http\Controllers;

use App\bookTitle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use PDF;
use Excel;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Support\Facades\Mail;

class bookTitleController extends Controller
{
    public function create(){
        return view('bookTitle.create');
    }
    public function  index(){

        $book_info = bookTitle::where('is_trashed','no')->paginate(3);;

        return view('bookTitle.index',compact('book_info',$book_info));
    }
    public function  store(Request $request){

        $this->validate($request, [
            'bookname' => 'required',
            'authorname' => 'required',
        ]);

        $bookTitle=new bookTitle();
        $bookTitle->book_name=$request->bookname;
        $bookTitle->author_name=$request->authorname;
        $bookTitle->save();
        return Redirect::to('/')->with('success',"Data Has Been Inserted Successfully");
    }
    public function  view($id){

        $single_info = bookTitle::find($id);

        return view('bookTitle.view',compact('single_info',$single_info));
    }
    public function  edit($id){
        $single_info = bookTitle::find($id);
        return view('bookTitle.edit',compact('single_info',$single_info));
    }
    public function  update(Request $request){
        $id=$request->id;

        $this->validate($request, [
            'bookname' => 'required',
            'authorname' => 'required',
        ]);
        $bookTitle=bookTitle::find($id);
        $bookTitle->book_name=$request->bookname;
        $bookTitle->author_name=$request->authorname;
        $bookTitle->update();
        return Redirect::to('/index')->with('success',"Data Has Been Inserted Successfully");
    }
    public function  delete($id){
        $single_info = bookTitle::find($id)->delete();
        return Redirect::to('/index')->with('success',"Data Has Been Inserted Successfully");
    }
    public function  trash($id){
        $bookTitle=bookTitle::find($id);
        $bookTitle->is_trashed='yes';
        $bookTitle->update();

        return Redirect::to('/index')->with('success',"Data Has Been Inserted Successfully");
    }
    public function  trashList(){

        $book_info = bookTitle::where('is_trashed','yes')->get();

        return view('bookTitle.trashed',compact('book_info',$book_info));
    }
    public function  recover($id){
        $bookTitle=bookTitle::find($id);
        $bookTitle->is_trashed='no';
        $bookTitle->update();

        return Redirect::to('/trashed')->with('success',"Data Has Been Inserted Successfully");
    }
    public function trashMultiple(Request $request){
        $Ids[]=$request->multiple;

        //DB::table('book_titles')->whereIn('id', $Ids)->delete();
        foreach($Ids as $id){
            DB::table('book_titles')->whereIn('id', $id)->update(['is_trashed' => 'yes']);

        }
        return Redirect::to('/index')->with('success',"Data Has Been Inserted Successfully");

    }
    public function deleteMultiple(Request $request){
        $path=$_SERVER['HTTP_REFERER'];
        $Ids[]=$request->multiple;

        foreach($Ids as $id){
            DB::table('book_titles')->whereIn('id', $id)->delete();
        }
        return Redirect::to($path)->with('success',"Data Has Been Inserted Successfully");

    }
    public function recoverMultiple(Request $request){
        $Ids[]=$request->multiple;

        foreach($Ids as $id){
            DB::table('book_titles')->whereIn('id', $id)->update(['is_trashed' => 'no']);

        }
        return Redirect::to('/trashed')->with('success',"Data Has Been Inserted Successfully");

    }
    public function pdf(){
        $book_info = bookTitle::where('is_trashed','no')->get();

        $pdf=PDF::loadView('bookTitle.pdf',['book_info'=>$book_info]);
        return $pdf->download('book_info.pdf');

    }
    public function excel(){


        $data = bookTitle::where('is_trashed','no')->get()->toArray();

        return Excel::create('book_title', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->export('xls');

    }
    public function mail()
    {
        //$data =  bookTitle::where('is_trashed','no')->get()->toArray();
$data=['1','2'];
        Mail::send('bookTitle.email', $data, function($message) use ($data)
        {
            $message->from('teamecho@gmail.com');
            $message->to('msisaifulsaif@gmail.com','Mohammed');
            $message->subject($data);
        });

       // return redirect('contactUs');


    }
}
