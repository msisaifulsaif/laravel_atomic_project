<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\profile_picture;
use DB;
use Illuminate\Support\Facades\Redirect;
use PDF;
use Excel;
class profilePicturController extends Controller
{
    public function create(){
        return view('profilePicture.create');
    }
    public function  index(){

        $image_info = profile_picture::where('is_trashed','no')->paginate(3);

        return view('profilePicture.index',compact('image_info',$image_info));
    }
    public function  store(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'image' => 'required',
        ]);
        $image= $request->file('image');
        $name=time().$image->getClientOriginalName();
        $uploadPath='public/img/';
        $image->move($uploadPath,$name);
        $imageUrl=$uploadPath.$name;

        $profile_picture=new profile_picture();
        $profile_picture->name=$request->name;
        $profile_picture->image=$imageUrl;
        $profile_picture->save();

        return Redirect::to('/pp')->with('success',"Data Has Been Inserted Successfully");
    }
    public function  view($id){

        $single_info = profile_picture::find($id);



        return view('profilePicture.view',compact('single_info',$single_info));
    }
    public function  edit($id){
        $single_info = profile_picture::find($id);
        return view('profilePicture.edit',compact('single_info',$single_info));
    }
    public function  update(Request $request){
        $id=$request->id;

        $this->validate($request, [
            'name' => 'required',
        ]);

        if ($request->image==''){
            $profilePicture=profile_picture::find($id);
            $profilePicture->name=$request->name;
            $profilePicture->update();
        }else{
            $image= $request->file('image');
            $name=time().$image->getClientOriginalName();
            $uploadPath='public/img/';
            $image->move($uploadPath,$name);
            $imageUrl=$uploadPath.$name;

            $profilePicture=profile_picture::find($id);
            unlink($profilePicture->image);
            $profilePicture->name=$request->name;
            $profilePicture->image=$imageUrl;
            $profilePicture->update();
        }
        return Redirect::to('/ppindex')->with('success',"Data Has Been Inserted Successfully");
    }
    public function  delete($id){
        $single_info = profile_picture::find($id)->delete();
        return Redirect::to('/ppindex')->with('success',"Data Has Been Inserted Successfully");
    }
    public function  trash($id){
        $profilePicture=profile_picture::find($id);
        $profilePicture->is_trashed='yes';
        $profilePicture->update();

        return Redirect::to('/ppindex')->with('success',"Data Has Been Inserted Successfully");
    }
    public function  trashList(){

        $image_info = profile_picture::where('is_trashed','yes')->paginate(3);


        return view('profilePicture.trashed',compact('image_info',$image_info));
    }
    public function  recover($id){
        $profilePicture=profile_picture::find($id);
        $profilePicture->is_trashed='no';
        $profilePicture->update();

        return Redirect::to('/pptrashed')->with('success',"Data Has Been Inserted Successfully");
    }
    public function trashMultiple(Request $request){
        $Ids[]=$request->multiple;

        foreach($Ids as $id){
            DB::table('profile_pictures')->whereIn('id', $id)->update(['is_trashed' => 'yes']);

        }
        return Redirect::to('/ppindex')->with('success',"Data Has Been Inserted Successfully");

    }
    public function deleteMultiple(Request $request){
        $path=$_SERVER['HTTP_REFERER'];
        $Ids[]=$request->multiple;

        foreach($Ids as $id){
            DB::table('profile_pictures')->whereIn('id', $id)->delete();
        }
        return Redirect::to($path)->with('success',"Data Has Been Inserted Successfully");

    }
    public function recoverMultiple(Request $request){
        $Ids[]=$request->multiple;

        foreach($Ids as $id){
            DB::table('profile_pictures')->whereIn('id', $id)->update(['is_trashed' => 'no']);

        }
        return Redirect::to('/pptrashed')->with('success',"Data Has Been Inserted Successfully");

    }
    public function pdf(){
        $book_info = profile_picture::where('is_trashed','no')->get();

        $pdf=PDF::loadView('profilePicture.pdf',['book_info'=>$book_info]);
        return $pdf->download('profilePicyure.pdf');

    }
    public function excel(){


        $data = profile_picture::where('is_trashed','no')->get()->toArray();
        return Excel::create('profile_picture', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->export('xls');

    }
    public function mail()
    {
        //$data =  profilePicture::where('is_trashed','no')->get()->toArray();
        $data=['1','2'];
        Mail::send('profilePicture.email', $data, function($message) use ($data)
        {
            $message->from('teamecho@gmail.com');
            $message->to('msisaifulsaif@gmail.com','Mohammed');
            $message->subject($data);
        });

        // return redirect('contactUs');


    }
}
