<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profile_picture extends Model
{
    protected $fillable = [
        'name', 'image',
    ];
}
