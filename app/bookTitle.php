<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bookTitle extends Model
{
    protected $fillable = [
        'book_name', 'author_name',
    ];
}
