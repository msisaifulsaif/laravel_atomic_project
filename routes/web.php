<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Book Title Routing Start
|--------------------------------------------------------------------------
*/

Route::get('/', 'bookTitleController@create');
Route::get('/index', 'bookTitleController@index');
Route::post('/storeBook', 'bookTitleController@store');
Route::get('/view/{id}', 'bookTitleController@view');
Route::get('/edit/{id}', 'bookTitleController@edit');
Route::get('/trash/{id}', 'bookTitleController@trash');
Route::get('/delete/{id}', 'bookTitleController@delete');
Route::post('/updateBook', 'bookTitleController@update');
Route::get('/trashed', 'bookTitleController@trashList');
Route::get('/recover/{id}', 'bookTitleController@recover');
Route::post('/trash_multiple', 'bookTitleController@trashMultiple');
Route::post('/delete_multiple', 'bookTitleController@deleteMultiple');
Route::post('/recover_multiple', 'bookTitleController@recoverMultiple');
Route::get('/pdf', 'bookTitleController@pdf');
Route::get('/xl', 'bookTitleController@excel');
Route::get('/mail', 'bookTitleController@mail');

/*
|--------------------------------------------------------------------------
| Book Title Routing End
|--------------------------------------------------------------------------
*/


/*
|--------------------------------------------------------------------------
| ProfilePicture Routing Start
| Here pp means=Profile Picture
|--------------------------------------------------------------------------
*/

Route::get('/pp', 'profilePicturController@create');
Route::get('/ppindex', 'profilePicturController@index');
Route::post('/ppstore', 'profilePicturController@store');
Route::get('/ppview/{id}', 'profilePicturController@view');
Route::get('/ppedit/{id}', 'profilePicturController@edit');
Route::get('/pptrash/{id}', 'profilePicturController@trash');
Route::get('/ppdelete/{id}', 'profilePicturController@delete');
Route::post('/ppupdate', 'profilePicturController@update');
Route::get('/pptrashed', 'profilePicturController@trashList');
Route::get('/pprecover/{id}', 'profilePicturController@recover');
Route::post('/pptrash_multiple', 'profilePicturController@trashMultiple');
Route::post('/ppdelete_multiple', 'profilePicturController@deleteMultiple');
Route::post('/pprecover_multiple', 'profilePicturController@recoverMultiple');
Route::get('/pppdf', 'profilePicturController@pdf');
Route::get('/ppxl', 'profilePicturController@excel');
Route::get('/ppmail', 'profilePicturController@mail');

/*
|--------------------------------------------------------------------------
| ProfilePicture Routing End
|--------------------------------------------------------------------------
*/