  <form  id="selectionForm" action="trash_multiple.php" method="post">

   <div class="nav navbar">

        <input class="btn btn-warning btn-lg" type="button" id="trashMultipleButton" value="Trash Multiple">
        <input class="btn  btn-danger btn-lg" type="button" id="deleteMultipleButton" value="Delete Multiple">

        <a href="pdf.php" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-list-alt"></span> <span class="glyphicon glyphicon-circle-arrow-down"></span> Download as PDF</a>
        <a href="xl.php" class="btn btn-lg btn-info"><span class="glyphicon glyphicon-list-alt"></span> <span class="glyphicon glyphicon-circle-arrow-down"></span> Download as Excel</a>
        <a href="email.php?list=1" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-list-alt"></span> <span class="glyphicon glyphicon-envelope"></span> Email This List</a>


   </div>