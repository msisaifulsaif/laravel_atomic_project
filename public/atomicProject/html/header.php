<div class="header">

    <nav class="navbar navbar-default">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span> class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#" style="color: black"><i> Atomic Project</i></a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="background-color:#1abc9c ">

            <ul class="nav navbar-nav pull-right" >
                <li><a href="http://localhost/TEAM_ECHO_B58_Atomic_Project/views/TEAM_ECHO/BookTitle/create.php" class="glyphicon glyphicon-book hm-green-light" style="color: black"><i>BookTitle</i></a></li>
                <li><a href="http://localhost/TEAM_ECHO_B58_Atomic_Project/views/TEAM_ECHO/Birthday/create.php" class="glyphicon glyphicon-calendar" style="color: black"><i>Birthday</i></a></li>
                <li><a href="http://localhost/TEAM_ECHO_B58_Atomic_Project/views/TEAM_ECHO/City/create.php" class="glyphicon glyphicon-globe" style="color: black"><i>City</i></a></li>
                <li><a href="http://localhost/TEAM_ECHO_B58_Atomic_Project/views/TEAM_ECHO/Gender/create.php" class="glyphicon glyphicon-user" style="color: black"><i>Gender</i></a></li>
                <li> <a href="http://localhost/TEAM_ECHO_B58_Atomic_Project/views/TEAM_ECHO/profilePicture/create.php" class="glyphicon glyphicon-picture" style="color: black"><i>ProfilePicture</i></a></li>
                <li><a href="http://localhost/TEAM_ECHO_B58_Atomic_Project/views/TEAM_ECHO/Hobbies/create.php" class="glyphicon glyphicon-camera" style="color: black"><i>Hobbies</i></a></li>
                <li><a href="http://localhost/TEAM_ECHO_B58_Atomic_Project/views/TEAM_ECHO/Organigation/create.php" class="glyphicon glyphicon-folder-open" style="color: black"><i>Organization</i></a></li>

            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</div><!-- /.container-fluid -->
